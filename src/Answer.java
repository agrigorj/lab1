import java.util.*;

public class Answer {

    public static void main(String[] param) {

       /** // TODO!!! Solutions to small problems
        //   that do not need an independent method!

        // conversion double -> String
        String str = new String();
        double d = 10.5;
        try {
            str = Double.toString(d);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // conversion String -> int
        int n1 = 0;
        try {
            n1 = Integer.parseInt("2916");
        } catch (NumberFormatException e) {
            System.out.println("Püüdsin kinni teisenduse vea.");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // "hh:mm:ss"
        DateFormat timef = new SimpleDateFormat("HH:mm:ss");
        Date time = new Date();
        System.out.println(timef.format(time));

        // cos 45 deg
        double angle = 45;
        System.out.println("cos 45 deg = " + Math.cos(Math.toRadians(angle)));


        // table of square roots
        for (int k = 0; k < 11; k++) {
            System.out.print("sqrt " + k + " = ");
            System.out.println(Math.sqrt(k));
        }  */

        String firstString = "ABcd12";
        String result = reverseCase(firstString);
        System.out.println("\"" + firstString + "\" -> \"" + result + "\"");

        // reverse string

        String s = "How  many	 words   here";
        int nw = countWords(s);
        System.out.println(s + "\t" + String.valueOf(nw));

        // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!



        final int LSIZE = 100;
        ArrayList<Integer> randList = new ArrayList(LSIZE);
        Random generaator = new Random();
        for (int i = 0; i < LSIZE; i++) {
            randList.add(new Integer(generaator.nextInt(1000)));
        }
        /**
        // minimal element
        int min = randList.get(0);
        for (int i : randList) {
            min = min < i ? min : i;
        }
        System.out.println("Minimum: " + min);



        // HashMap tasks:
        //    create
        //    print all keys
        //    remove a key
        //    print all pairs
        HashMap<String, Integer> a = new HashMap();
        a.put("Peeter", 35);
        a.put("Mari", 38);
        a.put("Janika", 15);
        a.put("Mikk", 3);
        for (HashMap.Entry<String, Integer> entry : a.entrySet()) {
            System.out.println(entry.getKey());

        }
        a.remove("Peeter");

        System.out.println(a);
         */

        System.out.println("Before reverse:  " + randList);
        reverseList(randList);
        System.out.println("After reverse: " + randList);

        System.out.println("Maximum: " + maximum(randList));
    }


    /**
     * Finding the maximal element.
     *
     * @param a Collection of Comparable elements
     * @return maximal element.
     * @throws NoSuchElementException if <code> a </code> is empty.
     */
    static public <T extends Object & Comparable<? super T>>
    T maximum(Collection<? extends T> a)
            throws NoSuchElementException {

        return Collections.max(a);
    }

    /**
     * Counting the number of words. Any number of any kind of
     * whitespace symbols between words is allowed.
     *
     * @param text text
     * @return number of words in the text
     */
    public static int countWords(String text) {
        int count = 0;
        char ch;
        for (int i = 0; i < text.length(); i++) {
            ch = text.charAt(i);
            if (Character.isLetterOrDigit(ch) || ch == '-') {
                //System.out.println("this is a word");
            } else if (i != 0 && Character.isLetterOrDigit(text.charAt(i - 1))) {
                //System.out.println("word ends, spacebar");
                count++;
            } else {
                //System.out.println("spacebar");
            }
        }
        if (Character.isLetterOrDigit(text.charAt(text.length() - 1))) {
            count++;
        }
        return count;
    }

    /**
     * Case-reverse. Upper -> lower AND lower -> upper.
     *
     * @param s string
     * @return processed string
     */
    public static String reverseCase(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isLowerCase(ch)) {
                sb.append(Character.toUpperCase(ch));
            } else {
                if (Character.isUpperCase(ch)) {
                    sb.append(Character.toLowerCase(ch));
                } else {
                    sb.append(ch);
                }
            }
        }
        return sb.toString();
    }


    /**
     * List reverse. Do not create a new list.
     *
     * @param list list to reverse
     */
    public static <T extends Object> void reverseList(List<T> list)
            throws UnsupportedOperationException {
        int end = list.size();
        for (int i = end - 1; i > -1; i--) {
            list.add(end, list.get(i));
            list.remove(i);
        }
    }
}
